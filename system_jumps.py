"""ESI Dumper
  
  This will take system jumps and insert them into a basic table.
  
  ESI only returns jumps that have systems, we'll add a zero for each of 
  those systems on run, to keep consistent data
"""
#!/usr/bin/python3

from os import environ, path
from datetime import datetime
import requests
from sqlalchemy import Table, Column, Integer, insert, MetaData, DateTime, create_engine

from dotenv import load_dotenv

# Load configuration values from the .env file
basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, ".env"))
SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI")

JUMPS_ENDPOINT = (
    "https://esi.evetech.net/latest/universe/system_jumps/?datasource=tranquility"
)

SYSTEMS_ENDPOINT = (
    "https://esi.evetech.net/latest/universe/systems/?datasource=tranquility"
)

metadata = MetaData()
system_jumps = Table(
    "system_jumps",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("datestamp", DateTime),
    Column("ship_jumps", Integer),
    Column("system_id", Integer),
)


engine = create_engine(SQLALCHEMY_DATABASE_URI)
connection = engine.connect()

jumps = requests.get(JUMPS_ENDPOINT)

systems = requests.get(SYSTEMS_ENDPOINT)


# Build list of all the systems requ
jump_systems = []
for system in jumps.json():
    jump_systems.append(system['system_id'])

# Find the list of systems without jumps
list_difference = list( set(systems.json()) - set(jump_systems))

dt = datetime.now()

# Insert systems reported that have jumps
JUMP_COUNT = 0
for row in jumps.json():
    stmt = insert(system_jumps).values(
        ship_jumps=row["ship_jumps"],
        system_id=row["system_id"],
        datestamp=dt,
    )
    connection.execute(stmt)
    JUMP_COUNT = JUMP_COUNT + 1

# Insert systems that have no jumps, but we want consistent data
for empty_system in list_difference:
    stmt = insert(system_jumps).values(
        ship_jumps=0,
        system_id=empty_system,
        datestamp=dt,
    )
    connection.execute(stmt)

print(f"Added {JUMP_COUNT} Systems and {len(list_difference)} empties")
