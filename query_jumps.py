#!/usr/bin/python3

from os import environ, path
from datetime import datetime
from sqlalchemy import Table, Column, Integer, insert, MetaData, DateTime, create_engine

from dotenv import load_dotenv

# Load configuration values from the .env file
basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, ".env"))
SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI")


engine = create_engine(SQLALCHEMY_DATABASE_URI)
connection = engine.connect()

def get_region_name(region_id):
  query = f"select regionName from mapRegions where regionID = {region_id}"
  result = connection.execute(query)
  row = result.fetchone()
  return row[0]


def get_rows():
  query = "select regionID,system_id,solarSystemName,security, sum(ship_jumps) as jc  from system_jumps sj INNER JOIN mapSolarSystems ms ON sj.system_id = ms.solarSystemID group by system_id having jc > 0 order by jc asc"

  result = connection.execute(query)

  
  return result.fetchall()

def main():

  rows = get_rows()
  for row in rows:
      region_id = row[0]
      system_name = row[2]
      system_security = row[3]
      system_jumps = row[4]

      region_name = get_region_name(region_id)
      print(f"{region_name},{system_name},{system_security},{system_jumps}")


if __name__ == '__main__':
   main()
    