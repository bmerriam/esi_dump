#!/usr/bin/python3


file = open('results.csv')


lines = file.readlines()

for line in lines:
    line = line.replace('\n', '').split(",")
    #print(line)
    region_name = line[0]
    system_name = line[1]
    system_security = float(line[2])
    system_jumps = line[3]
    #print(system_security)
    if (system_security < 0.47) and (system_security > 0.13):
        print(f"{region_name},{system_name},{system_security},{system_jumps}") 
